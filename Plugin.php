<?php namespace PlanetaDelEste\Statuses;

use Backend;
use System\Classes\PluginBase;

/**
 * Statuses Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'planetadeleste.statuses::lang.plugin.name',
            'description' => 'planetadeleste.statuses::lang.plugin.description',
            'author'      => 'PlanetaDelEste',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'PlanetaDelEste\Statuses\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'planetadeleste.statuses.access' => [
                'tab'   => 'planetadeleste.statuses::lang.plugin.name',
                'label' => 'planetadeleste.statuses::lang.permissions.access'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'statuses' => [
                'label'       => 'planetadeleste.statuses::lang.plugin.name',
                'url'         => Backend::url('planetadeleste/statuses/actions'),
                'icon'        => 'icon-check-square-o',
                'permissions' => ['planetadeleste.statuses.*'],
                'order'       => 500,
                'sideMenu'    => [
                    'add' => [
                        'label'       => 'planetadeleste.statuses::lang.action.new',
                        'url'         => Backend::url('planetadeleste/statuses/actions/create'),
                        'icon'        => 'icon-plus',
                        'permissions' => ['planetadeleste.statuses.*'],
                        'group'       => 'planetadeleste.statuses::lang.plugin.name',
                        'description' => '',
                    ],
                    'actions' => [
                        'label'       => 'planetadeleste.statuses::lang.actions.menu_label',
                        'url'         => Backend::url('planetadeleste/statuses/actions'),
                        'icon'        => 'icon-check-square-o',
                        'permissions' => ['planetadeleste.statuses.*'],
                        'group'       => 'planetadeleste.statuses::lang.plugin.name',
                        'description' => '',
                    ]
                ]
            ],
        ];
    }

}
